"use strict";

var name = 'Regina';
var html = '';
var data = [{
  name: 'Regina',
  base: 'tomate',
  price_small: 6.5,
  price_large: 9.95,
  image: 'https://images.unsplash.com/photo-1532246420286-127bcd803104?fit=crop&w=500&h=300'
}, {
  name: 'Napolitaine',
  base: 'tomate',
  price_small: 6.5,
  price_large: 8.95,
  image: 'https://images.unsplash.com/photo-1562707666-0ef112b353e0?&fit=crop&w=500&h=300'
}, {
  name: 'Spicy',
  base: 'crème',
  price_small: 5.5,
  price_large: 8,
  image: 'https://images.unsplash.com/photo-1458642849426-cfb724f15ef7?fit=crop&w=500&h=300'
}]; //const data = ['Regina','Napolitaine','Spicy'];

for (var pas = 0; pas < data.length; pas++) {
  //  const url = 'images/'+data[pas].toLowerCase()+'.jpg';
  var url = data[pas].image;
  html += '<article class ="pizzaThumbnail"><a href="' + url + '">' + '<img src="' + url + '"/><' + 'section>' + '<h4>' + data[pas].name + '</h4>' + '<ul> <li>Prix petit format :' + data[pas].price_small + '€ </li> <li>Prix grand format : ' + data[pas].price_large + '€ </li> </ul>' + '</section>' + '</a>\n</article>';
} // const map = data.map(element => '<article class ="pizzaThumbnail"> \n \t <a href="'+'images/'+element.toLowerCase()+'.jpg'+'">'+'\n \t \t<img src="'+'images/'+element.toLowerCase()+'.jpg'+'"/> \n \t \t<'+'section>'+element+'</section>'+'\n \t</a>\n</article>\n')
// for (let pas = 0 ; pas < map.length ; pas++) {
//    html += map [pas];
// }


console.log(html);
document.querySelector('.pageContent').innerHTML = html;